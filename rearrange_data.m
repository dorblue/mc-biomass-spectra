function [newData] = rearrange_data (oldData)

[nrow, ncol] = size(oldData);

newData = zeros(nrow, ncol);

newData(1, :) = oldData(1, :);
newData(7, :) = oldData(2, :);
newData(13, :) = oldData(3, :);
newData(19, :) = oldData(4, :);
newData(25, :) = oldData(5, :);
newData(31, :) = oldData(6, :);
newData(37, :) = oldData(7, :);

newData(2, :) = oldData(8, :);
newData(8, :) = oldData(9, :);
newData(14, :) = oldData(10, :);
newData(20, :) = oldData(11, :);
newData(26, :) = oldData(12, :);
newData(32, :) = oldData(13, :);
newData(38, :) = oldData(14, :);

newData(3, :) = oldData(15, :);
newData(9, :) = oldData(16, :);
newData(15, :) = oldData(17, :);
newData(21, :) = oldData(18, :);
newData(27, :) = oldData(19, :);
newData(33, :) = oldData(20, :);
newData(39, :) = oldData(21, :);

newData(4, :) = oldData(22, :);
newData(10, :) = oldData(23, :);
newData(16, :) = oldData(24, :);
newData(22, :) = oldData(25, :);
newData(28, :) = oldData(26, :);
newData(34, :) = oldData(27, :);
newData(40, :) = oldData(28, :);

newData(5, :) = oldData(29, :);
newData(11, :) = oldData(30, :);
newData(17, :) = oldData(31, :);
newData(23, :) = oldData(32, :);
newData(29, :) = oldData(33, :);
newData(35, :) = oldData(34, :);
newData(41, :) = oldData(35, :);

newData(6, :) = oldData(36, :);
newData(12, :) = oldData(37, :);
newData(18, :) = oldData(38, :);
newData(24, :) = oldData(39, :);
newData(30, :) = oldData(40, :);
newData(36, :) = oldData(41, :);
newData(42, :) = oldData(42, :);