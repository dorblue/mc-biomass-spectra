function [] = plot_replicates(data, days, replicates)

% - - - Plotting Euclidean distances (replicates of the same sample) - - -
figure;
for i = 1: days
    subplot(4, 2, i);
    scatter(1: replicates, data(i, :));
    ylim([0 max(data(i, :)) + .2]);
    title ("Day " + i);
    
    hold on;
    y(1: replicates) = mean(data(i, :));
    plot(1: replicates, y);
    hold off;
end