function [spCut] = cut_range(sp, wn1, wn2)

wns = str2num(sp.v);
[~,i2] = min(abs(wns - wn1));
[~,i1] = min(abs(wns - wn2));
spCut = selectcol(sp,[i1:i2]);

end