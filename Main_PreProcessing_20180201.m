%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                                      %
%            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%                                        %
%            %                                %                                        %
%            %           --- MAIN ---         %                                        %
%            %                                %                                        %
%            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%                                        %
%                                                                                      %
%                                                                                      %
%  Achim Kohler                                                                        %
%  Faculty of Science and Technology (RealTek)                                         %
%  Norwegian Unversity of Life Sciences (www.nmbu.no)                                  %
%                                                                                      %
%                                                                                      %
%  Post address:                                                                       %
%                                                                                      %
%  PO Box 5003, 1432 Aas, Norway                                                       %
%                                                                                      %
%  First version:   24.12.2017                                                         %
%                                                                                      %
%                                                                                      %
%                                                                                      %
%                                                                                      %
%                                                                                      %
%--------------------------------------------------------------------------------------%
%                                                                                      % 
%  status: working                                                                     %
%                                                                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear variables;
close all;

fileName_D5 = ["MC_D5_0.mat", "MC_D5_025.mat", "MC_D5_05.mat", "MC_D5_1.mat", "MC_D5_2.mat", "MC_D5_4.mat"];
fileName_D7 = ["MC_D7_0.mat", "MC_D7_025.mat", "MC_D7_05.mat", "MC_D7_1.mat", "MC_D7_2.mat", "MC_D7_4.mat"];

numbOfSampls = length(fileName_D7);
numbOfDays = 7;
numbOfWns = 1557;

% spData = [];
% spDataMean = [];
% wnsData = [];
% smplData = [];

ZCorrMean = struct('d', {}, 'i', {}, 'v', {});
ZCorrMean(1).d = zeros(numbOfDays * numbOfSampls, numbOfWns);
ZCorrMean(1).v = char(numbOfWns, 8);
ZCorrMean(1).i = char(zeros(numbOfDays * numbOfSampls, 9));

for i = 1: numbOfSampls
    ZCorrected = load_many_files(fileName_D7(i));
    ZMean = average_spectra(ZCorrected);
    
    if (i == 1)
        ZCorrMean.v = ZMean.v;
    end 
    [~, nameL] = size(ZMean.i);
    ZCorrMean.i(((i - 1) * numbOfDays + 1): (i * numbOfDays), 1: nameL) = ZMean.i;
    ZCorrMean.d(((i - 1) * numbOfDays + 1): (i * numbOfDays), :) = ZMean.d; 
    
    clear ZCorrected ZMean;
end

ZCorrMeanArr = rearrange_mean_data (ZCorrMean);

daysArr = zeros(numbOfSampls * numbOfDays, 1);
for i = 1: (numbOfSampls * numbOfDays)
    daysArr(i) = ZCorrMeanArr.i(i, 5) - '0';
end

% close all;

ZCorrMeanArr_1 = cut_range(ZCorrMeanArr, 2600, 3200);

figure;
group_plot(ZCorrMeanArr_1, daysArr, 'All spectral data - cultivation day 5');

disp ("BP 2");

if (0)
    % Select specific region
    UpperWn=1000.0;
    LowerWn=1800.0;
    [y,i2]=min(abs(str2num(ZCorrected.v)-UpperWn));
    [y,i1]=min(abs(str2num(ZCorrected.v)-LowerWn));
    ZCorrected1=selectcol(ZCorrected,[i1:i2]);
    
    UpperWn=2800.0;
    LowerWn=3100.0;
    [y,i2]=min(abs(str2num(ZCorrected.v)-UpperWn));
    [y,i1]=min(abs(str2num(ZCorrected.v)-LowerWn));
    ZCorrected2=selectcol(ZCorrected,[i1:i2]);
    
    ZCorrected=appendcol(ZCorrected1,ZCorrected2);
end

if (1)
    PCA_mod=pca(ZCorrected);

    x=1; y=2;  % Components
    figure;
    subplot(1,2,1)
    carte_couleur1(PCA_mod.score,x,y,1,4);
    set(gcf,'Color',[1 1 1]);
    FontSize=18;
    title(texlabel(''));
    subplot(1,2,2)
    x=3; y=4;  % Components
    carte_couleur1(PCA_mod.score,x,y,1,4);
    set(gcf,'Color',[1 1 1]);
    FontSize=18;
    title(texlabel(''));
end





