function [spectra] = load_many_files (file_name)

close all;

% To do or not to do replicate correction
plotid=1;
RepCor=1;

% To do or not to do quality testing
qualityTest = 0;

%% Set path to ako's functions
Akos=genpath('C:\Users\darya\OneDrive\PhD\Codes\Darya\biospec_functions');
addpath(Akos,'C:\Users\darya\OneDrive\PhD\Codes\Darya\saisir');
addpath(Akos,'C:\Users\darya\OneDrive\PhD\Codes\Darya\darya_functions');

DirNameSaisir='C:\Users\darya\OneDrive\PhD\Codes\Darya\MC biomass spectra_Matlab\DAY 7\';


%% Load file
[ZSaisir1]=LoadFromUnscrambler_v1004(DirNameSaisir, file_name);

% 1st and 2nd derivatives
ZSaisir1_1=saisir_derivative(ZSaisir1,2,9,1);
ZSaisir1_2=saisir_derivative(ZSaisir1,2,9,2); 

% Select specific region
% upper wn < lower wn!
UpperWn=1000.0;   
LowerWn=4000.0;

% wns = str2num(ZSaisir1.v); 
ZFTIR1 = cut_range(ZSaisir1, 1000, 4000);

[numbOfSampls, numbOfWns] = size(ZFTIR1.d);

% - - - - - Obtaining the day this very spectrum was measured - - - - - %
[samples, ~] = size(ZFTIR1.i);
daysArr = zeros(samples, 1);
for s = 1:samples
    sampleN = ZFTIR1.i(s, :);
    daysArr(s) = str2double(sampleN(5));
end
numbOfDays = max(daysArr);

% Group plotting routine (raw spectra, 1st and 2nd derivatives)
figure;
% group_plot(ZFTIR1, daysArr);
% group_plot(ZFTIR1_1, daysArr);
% group_plot(ZFTIR1_2, daysArr);

if (qualityTest) 
    % - - - Quality tests) - - -
    metrics_test(ZFTIR1, daysArr);

    % - - - Quality test (between 3200 cm-1 and 2600 cm-1) - - -
    metrics_test(ZFTIR1, daysArr, 2600, 3200);

    % - - - Quality test (between 1800 cm-1 and 1700 cm-1) - - -
    metrics_test(ZFTIR1, daysArr, 1700, 1800);

    % - - - Quality test (between 1200 cm-1 and 700 cm-1) - - -
    metrics_test(ZFTIR1, daysArr, 700, 1200);
end

% Define weights
EMSCWeights.d=ones(1,numbOfWns);    
EMSCWeights.v=ZFTIR1.v; 
EMSCWeights.i='EMSCWgts';

if (RepCor)
    % - - - EMSC correction with replicates
    [EMSCModel] = make_emsc_rep_mod(ZFTIR1,1,7,1,2,EMSCWeights,plotid);
else
    % - - - EMSC correction w/o replicates
    [EMSCModel] = make_emsc_modfunc(ZFTIR1);  %% We use the January set as Calibration set
end

if (0)
    figure;
    plot(str2num(EMSCModel.ModelVariables),EMSCModel.Model'); 
    set(gca,'XDir','reverse');
    set(gcf,'Color',[1 1 1]);
    xlabel('Wavenumber [cm^-^1]','FontSize',8);
    title('Raw spectra');
    axis tight;
end

[ZCorrected,ZResiduals,EMSCParam]=cal_emsc(ZFTIR1,EMSCModel,[],[]);

% - - - Compare corrected spectra with the raw ones - - -
% UpperWn=800.0;   
% LowerWn=3200.0;
% 
% [~,i2]=min(abs(str2num(ZCorrected.v)-UpperWn));
% [~,i1]=min(abs(str2num(ZCorrected.v)-LowerWn));
% ZCorrected=selectcol(ZCorrected,[i1:i2]);
% ZFTIR1_1=selectcol(ZFTIR1,[i1:i2]);
ZFTIR1_1 = ZFTIR1;

if (0)
    figure;
    subplot(1, 2, 1);
    group_plot(ZCorrected, daysArr, "Corrected spectra");
    
    subplot(1, 2, 2);
    group_plot(ZFTIR1_1, daysArr, "Raw spectra");
end

% - - - Plotting replicate data before and after EMSC correction - - -
% 1. The whole spectral region
% metrics_test(ZFTIR1_1, daysArr, "Raw spectra (4000 - 1000 cm-1)");
% metrics_test(ZCorrected, daysArr, "Corrected spectra (4000 - 1000 cm-1)");
% metrics_test_comp(ZFTIR1_1, ZCorrected, daysArr, "Raw vs. corrected spectra (4000 - 1000 cm-1)");

% 2. Lipid region (1800 - 1700 cm-1)
% metrics_test(ZFTIR1_1, daysArr, 1700, 1800, "Raw spectra (1800 - 1700 cm-1)");
% metrics_test(ZCorrected, daysArr, 1700, 1800, "Corrected spectra (1800 - 1700 cm-1)");
% metrics_test_comp(ZFTIR1_1, ZCorrected, daysArr, 1700, 1800, "Raw vs. corrected spectra (1800 - 1700 cm-1)");

% 3. Saturation region (3200 - 2600 cm-1)
% metrics_test(ZFTIR1_1, daysArr, 2600, 3200, "Raw spectra (3200 - 2600 cm-1)");
% metrics_test(ZCorrected, daysArr, 2600, 3200, "Corrected spectra (3200 - 2600 cm-1)");
% metrics_test_comp(ZFTIR1_1, ZCorrected, daysArr, 2600, 3200, "Raw vs. corrected spectra (3200 - 2600 cm-1)");

disp("BP");

% - - - Averaging spectra by replicants - - -
ZFTIRMean = ZFTIR1_1;
ZFTIRMean.d = zeros(numbOfDays, numbOfWns);
% zMean = zeros(numbOfDays, numbOfWns);

figure;
for i = 1: numbOfDays
    ZFTIRMean.d(i, :) = mean(ZCorrected.d(((3*(i - 1) + 1): (3*i)), :), 1);
    set(gca,'XDir','reverse');
    set(gcf,'Color',[1 1 1]);
    xlabel('Wavenumber [cm^-^1]','FontSize',8);
    title('Averaged spectra');
    plot(str2num(ZCorrected.v), ZFTIRMean.d(i, :));
    axis tight;
    hold on;
end

hold off;

% - - - Plotting averaged spectra within different spectral ranges - - -
daysArr = (1:7);
% 1800 - 1700 cm-1
    UpperWn=1700.0;
    LowerWn=1800.0;
    [~,i2]=min(abs(str2num(ZFTIRMean.v)-UpperWn));
    [~,i1]=min(abs(str2num(ZFTIRMean.v)-LowerWn));
    ZMean_2=selectcol(ZFTIRMean,[i1:i2]);

%     figure;
%     group_plot(ZMean_2, daysArr, "Corrected spectra (1800 - 1700 cm-1)");
    
if (0)
    % 3200 - 2600 cm-1
    UpperWn=2600.0;
    LowerWn=3200.0;
    [~,i2]=min(abs(str2num(ZFTIRMean.v)-UpperWn));
    [~,i1]=min(abs(str2num(ZFTIRMean.v)-LowerWn));
    ZMean_1=selectcol(ZFTIRMean,[i1:i2]);
    
    figure;
    subplot(2, 2, 1);
    group_plot(ZMean_1, daysArr, "Corrected spectra (3200 - 2600 cm-1)");
    
    % 1800 - 1700 cm-1
    UpperWn=1700.0;
    LowerWn=1800.0;
    [~,i2]=min(abs(str2num(ZFTIRMean.v)-UpperWn));
    [~,i1]=min(abs(str2num(ZFTIRMean.v)-LowerWn));
    ZMean_2=selectcol(ZFTIRMean,[i1:i2]);
    
    subplot(2, 2, 2);
    group_plot(ZMean_2, daysArr, "Corrected spectra (1800 - 1700 cm-1)");
    
%     3100 - 2800
%     1800 - 1700
%     1400 - 700

% Variation within replicates and groups

    % 1200 - 700 cm-1
    UpperWn=700.0;
    LowerWn=1200.0;
    [~,i2]=min(abs(str2num(ZFTIRMean.v)-UpperWn));
    [~,i1]=min(abs(str2num(ZFTIRMean.v)-LowerWn));
    ZMean_3=selectcol(ZFTIRMean,[i1:i2]);
    
    subplot(2, 2, 3);
    group_plot(ZMean_3, daysArr, "Corrected spectra (1200 - 700 cm-1)");
end

disp("BP");

% spectra = ZFTIRMean;
spectra = ZCorrected;
