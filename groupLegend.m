%
% groupLegend By Morten Juelsgaard
% Last edit 06/08/2012
%
% Grouping multiple plots into one legend
%
% Syntax:
% groupLegend(pl_1,pl_2,...,pl_n)
%
% where pl_1,...,pl_n are arrays of graphic-handles to be grouped together,
% such that all handles in pl_1 appears as one legend, all handles in pl_2
% appears as one legend, etc.
%
% Demonstration:
% The function call
%   groupLegend('example')
% runs the following example code:
%
%    close all;
%    rng('default');
%    m = 100;
%    n = 10;
%    x = 1:m;
%    y = zeros(n,m); z = zeros(n,m);
%    for i = 2:m;
%        y(:,i) = y(:,i-1) + .5 + .1*randn(n,1);
%        z(:,i) = z(:,i-1) + .1 + randn(n,1);
%    end
%    
%    pl1 = plot(x,z,'r'); hold on;
%    pl2 = plot(x,y,'b'); 
%    xlim([1 100]);
%    xlabel('x');
%    ylabel('\Sigma_{i=1}^x N_i(\mu,\sigma^2)');
% 
%    groupLegend(pl1,pl2);
%
%    legend('\mu = .1, \sigma^2 = .01',...
%           '\mu = .5, \sigma^2 = 1','Location','NorthWest')   

function groupLegend(varargin)

if(strcmp(varargin{1},'example'))
   % Example     
   close all;
   rng('default');
   m = 100;
   n = 10;
   x = 1:m;
   y = zeros(n,m); z = zeros(n,m);
   for i = 2:m;
       y(:,i) = y(:,i-1) + .5 + .1*randn(n,1);
       z(:,i) = z(:,i-1) + .1 + randn(n,1);
   end
   
   pl1 = plot(x,z,'r'); hold on;
   pl2 = plot(x,y,'b'); 
   xlim([1 100]);
   xlabel('x')
   ylabel('\Sigma_{i=1}^x N_i(\mu,\sigma^2)')

   groupLegend(pl1,pl2);
   legend('\mu = .1, \sigma^2 = .01',...
          '\mu = .5, \sigma^2 = 1', 'Location','NorthWest');
else
    for i = 1:length(varargin)
        curPlotSet = hggroup;
        set(varargin{i},'Parent',curPlotSet);
        set(get(get(curPlotSet,'Annotation'),'LegendInformation'),...
            'IconDisplayStyle','on');
    end
end
    
end