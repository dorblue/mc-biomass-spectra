function [] = corr_btw_samples(ZFTIR1,numbOfSampls)

% - - - - - Inter-replicate quality assessment  - - - - -
replCorrMatr = ones(numbOfSampls, numbOfSampls);
for i = 1: (numbOfSampls - 1)
    for j = (i + 1): numbOfSampls
        replCorr = corrcoef(ZFTIR1.d(i, :), ZFTIR1.d(j, :));
        replCorrMatr(i, j) = replCorr(1, 2);
        replCorrMatr(j, i) = replCorr(2, 1);
    end
end

figure;
histogram (replCorrMatr, 'BinLimits', [0.9, 1]);
axis tight;
disp("Hello world!");

end

