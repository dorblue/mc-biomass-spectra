function [] = rubbish()

for i = 1: (numbOfSampls - 1)
    for j = (i + 1): numbOfSampls
        replCorr = (sum(ZFTIR1.d(i, :) .* ZFTIR1.d(j, :)) - (1/n)*sum(ZFTIR1.d(i, :))*sum(ZFTIR1.d(j, :)))/...
            sqrt(((sum((ZFTIR1.d(i, :)).^2)) - (1/n)*((sum(ZFTIR1.d(i, :))^2))) * ...
            ((sum((ZFTIR1.d(j, :)).^2)) - (1/n)*((sum(ZFTIR1.d(j, :))^2))));
        replCorrMatr_1(i, j) = replCorr;
    end
end


% - - - - - Quality test (between the days) - - - - -
UpperWn=1700.0;   
LowerWn=1800.0;

[~,i2]=min(abs(str2num(ZFTIR1.v)-UpperWn));
[~,i1]=min(abs(str2num(ZFTIR1.v)-LowerWn));
ZCorrected=selectcol(ZFTIR1,[i1:i2]);

figure;
for i = 1: numbOfDays
    meanSpec = mean(ZCorrected.d(((3*(i - 1) + 1): (3*i)), :), 1);
    set(gca,'XDir','reverse');
    set(gcf,'Color',[1 1 1]);
    xlabel('Wavenumber [cm^-^1]','FontSize',8);
    title('Raw spectra');
    axis tight;
    plot(str2num(ZCorrected.v), meanSpec);
    hold all;
end

