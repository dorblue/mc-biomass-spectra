function [ZCorrMean] = average_spectra (ZCorrected)  

numbOfDays = 7;
[numbOfSampls, numbOfWns] = size(ZCorrected.d);

ZCorrMean = struct('d', {}, 'i', {}, 'v', {});
ZCorrMean(1).d = zeros(numbOfDays, numbOfWns);
ZCorrMean(1).v = ZCorrected.v;

numbOfRepls = zeros(numbOfDays, 1);
samplsDayNames = char(zeros(numbOfDays, length(ZCorrected.i(1, :)) - 2));

for i = 1: numbOfSampls
    currFileName = ZCorrected.i(i, :);
    % Position where the day number occurs
    currDay = currFileName(5) - '0';
    % Saving spectral data to be averaged later
    numbOfRepls(currDay) = numbOfRepls(currDay) + 1;
    ZCorrMean.d(currDay, :) = ZCorrMean.d(currDay, :) + ZCorrected.d(i, :);
    if (samplsDayNames(currDay, :) == 0)
        samplsDayNames(currDay, :) = currFileName(1: (length(currFileName) - 2));
    end
end

ZCorrMean(1).i = samplsDayNames;

ZCorrMean.d = ZCorrMean.d./numbOfRepls;