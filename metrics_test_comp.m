function [] = metrics_test_comp(ZFTIR1, ZFTIR2, daysArr, upperWn, lowerWn, plot_title)

% - - - - - Quality test (between the days) - - - - -
if (exist('upperWn', 'var') && exist('lowerWn', 'var'))
    [~,i2] = min(abs(str2num(ZFTIR1.v)-upperWn));
    [~,i1] = min(abs(str2num(ZFTIR1.v)-lowerWn));
    ZFTIR1 = selectcol(ZFTIR1,(i1:i2));
    ZFTIR2 = selectcol(ZFTIR2,(i1:i2));
end

[numbOfSampls, ~] = size(ZFTIR1.d);
numbOfDays = 7;
numbOfRepls = 3;
diffMeasureArr1 = [];
diffMeasureArr2 = [];
for i = 1: numbOfSampls
    for j = (i + 1): (numbOfRepls * daysArr(i))
        % "chebychev" goes for the maximum coordinate difference
        % "euclidean" is used as default
        diffMeasureArr1 = cat(2, diffMeasureArr1, pdist2(ZFTIR1.d(i, :), ZFTIR1.d(j, :), 'euclidean'));
        diffMeasureArr2 = cat(2, diffMeasureArr2, pdist2(ZFTIR2.d(i, :), ZFTIR2.d(j, :), 'euclidean'));
    end
end

diffMeasureArr1 = (reshape(diffMeasureArr1, numbOfRepls, numbOfDays))';
diffMeasureArr2 = (reshape(diffMeasureArr2, numbOfRepls, numbOfDays))';

% - - - Plotting Euclidean distances (replicates of the same sample) - - -
yMin = min(min(min(diffMeasureArr1, diffMeasureArr2)));
yMax = max(max(max(diffMeasureArr1, diffMeasureArr2)));

groupArr = [];
colorArr = get(0,'DefaultAxesColorOrder');
figure;
for i = 1: numbOfDays
    subplot(4, 2, i);
    
%     yMin = min(min(diffMeasureArr1(i, :)), min(diffMeasureArr2(i, :)));
%     yMax = max(max(diffMeasureArr1(i, :)), max(diffMeasureArr2(i, :)));
    groupArr = cat(1, groupArr, scatter(1: numbOfRepls, diffMeasureArr1(i, :), 20, colorArr(1, :), 'filled', 'd'));
    ylim([0 (yMax + yMin)]);
    title ("Day " + i);
    
    hold on;
    y(1: numbOfRepls) = mean(diffMeasureArr1(i, :));
    plot(1: numbOfRepls, y);    
    
    hold on;
    groupArr = cat(1, groupArr, scatter(1: numbOfRepls, diffMeasureArr2(i, :), 20, colorArr(2, :), 'filled', 'o'));
    ylim([0 (yMax + yMin)]);
    title ("Day " + i);
    
    hold on;
    y(1: numbOfRepls) = mean(diffMeasureArr2(i, :));
    plot(1: numbOfRepls, y);
    
    hold off;
end

if (exist('plot_title', 'var'))
    annotation('textbox', [0 0.9 1 0.1], ...
        'String', plot_title, ...
        'FontSize', 12, ...
        'EdgeColor', 'none', ...
        'HorizontalAlignment', 'center');
end

legend(groupArr((1:length(groupArr))), char('Raw spectra', 'Corrected spectra'));

end