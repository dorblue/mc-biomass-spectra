function [ZCorrMeanArr] = rearrange_mean_data (ZCorrMean)

numbOfDays = 7;
numbOfSampls = 6;
[numbOfArrSampls, numbOfWns] = size(ZCorrMean.d);

ZCorrMeanArr = struct('d', {}, 'i', {}, 'v', {});
ZCorrMeanArr(1).d = zeros(numbOfArrSampls, numbOfWns);
ZCorrMeanArr(1).v = ZCorrMean.v;
ZCorrMeanArr(1).i = ZCorrMean.i;

samplsByDay = zeros(numbOfDays, 1);
for i = 1: numbOfArrSampls
    % Find the day this very experiment was conducted
    currDay = ZCorrMean.i(i, 5) - '0';
    
    samplsByDay(currDay) = samplsByDay(currDay) + 1;
    newPos = numbOfSampls*(currDay - 1) + samplsByDay(currDay);
    
    ZCorrMeanArr.d(newPos, :) = ZCorrMean.d(i, :);
    ZCorrMeanArr.i(newPos, :) = ZCorrMean.i(i, :);
end

disp("BP");