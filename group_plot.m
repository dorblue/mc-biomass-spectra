function [] = group_plot(ZFTIR1, daysArr, plot_title, case_name)

% - - - - - Plotting routine - - - - - %
% figure
set(gca,'XDir','reverse');
set(gcf,'Color',[1 1 1]);
xlabel('Wavenumber [cm^-^1]','FontSize',8);
if (exist('plot_title', 'var'))
    title(plot_title);
else    
    title('Raw spectra');
end

axis tight;
hold all;

samples = length(daysArr);

lastDay = max(daysArr);
% colorArr = zeros(lastDay, 3);
colorArr = get(0,'DefaultAxesColorOrder');
groupArr = gobjects(samples, 1);
wns = str2num(ZFTIR1.v);

for currDay = 1:lastDay
    % colorArr(currDay, :) = rand(1,3);
    numbOfSamples = sum(daysArr==currDay');
    if (not(exist('case_name', 'var')))
        for i = 1:numbOfSamples
            j = numbOfSamples*(currDay - 1) + i;
            newPlot = plot(wns,ZFTIR1.d(j, :), 'color', colorArr(currDay, :));
            groupArr(j) = newPlot;
        end
    else
        sampleDays = find(endsWith(ZFTIR1.i, string(currDay)) == 1);
        for j = 1: length(sampleDays)
            groupArr(sampleDays(j)) = plot(wns,ZFTIR1.d(sampleDays(j), :), 'color', colorArr(currDay, :));
        end
    end
end

dayNames = strcat('day', num2str((1:lastDay)'));
legend(groupArr((1:samples)),dayNames);
hold off;