function [] = metrics_test(ZFTIR1, daysArr, upperWn, lowerWn, plot_title)

    % - - - - - Quality test (between the days) - - - - -
    if (nargin > 3)
        [~,i2] = min(abs(str2num(ZFTIR1.v)-upperWn));
        [~,i1] = min(abs(str2num(ZFTIR1.v)-lowerWn));
        ZFTIR1 = selectcol(ZFTIR1,(i1:i2));
    end

    [numbOfSampls, numbOfWns] = size(ZFTIR1.d);
    numbOfDays = 7;
    numbOfRepls = 3;
    diffMeasureArr = [];
    for i = 1: numbOfSampls
        for j = (i + 1): (numbOfRepls * daysArr(i))
            % "chebychev" goes for the maximum coordinate difference
            % "euclidean" is typically used
            diffMeasureArr = cat(2, diffMeasureArr, pdist2(ZFTIR1.d(i, :), ZFTIR1.d(j, :), 'chebychev'));
        end
    end
    diffMeasureArr = (reshape(diffMeasureArr, numbOfRepls, numbOfDays))';

    % - - - Plotting Euclidean distances (replicates of the same sample) - - -
    figure;
    for i = 1: numbOfDays
        subplot(4, 2, i);
        scatter(1: numbOfRepls, diffMeasureArr(i, :));
        ylim([0 max(diffMeasureArr(i, :)) + .05]);
        title ("Day " + i);

        hold on;
        y(1: numbOfRepls) = mean(diffMeasureArr(i, :));
        plot(1: numbOfRepls, y);
        hold off;
    end

    if (exist('plot_title', 'var'))
        annotation('textbox', [0 0.9 1 0.1], ...
            'String', plot_title, ...
            'FontSize', 12, ...
            'EdgeColor', 'none', ...
            'HorizontalAlignment', 'center');
    end

end

